# !!! THIS PLUGIN IS NO LONGER MAINTAINED !!! #

Moving forward, the functionality of this plugin has been merged into the [Confluence JIRA Plugin](https://bitbucket.org/atlassian/confluence-jira-plugin).

# The Insert JIRA issue dialog box

This repository contains the plugin used for integrating JIRA issues into Confluence pages.

## Running the integration tests
	
	mvn clean verify

This will compile and package the plugin, start up JIRA and Confluence in the configured versions and run the integration tests (Selenium 1) against them. If you want to specify the target Confluence version for the run, use e.g.

	mvn clean verify -Dconfluence.version=4.3.3

If you want to run single integration tests from your IDE, run 

	mvn clean confluence:run -DtestGroup=conf_jira

first. This will just start up JIRA and Confluence and wait.

## For Atlassians

* [Confluence Stable Plan](https://confluence-bamboo.atlassian.com/browse/CONFPLGSTB-JC)
* [Confluence Master Plan](https://confluence-bamboo.atlassian.com/browse/CONFPLGTRK-JC)